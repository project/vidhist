<?php
/**
 * @file
 * Services callbacks for vidhist.module.
 */

/**
 * Callback to create a video viewing record.
 *
 * Example Request Header
 * ----------------------
 * Accept: application/json
 * Content-Type: application/x-www-form-urlencoded
 *
 * Example Request Body
 * --------------------
 * Request Url: http://d7.drupalize.lan/api/vidhist
 * Request Method: POST
 * Status Code: 200
 * Params: {
 *  "nid": "692",
 *  "uid": "9",
 *  "start": "0",
 *  "end": "20",
 *  "duration": "666"
 * }
 *
 * @param $data
 * @return array
 *   id: ID of the newly created record.
 *   uri: URI of the record.
 */
function _vidhist_resource_create($data) {
  $transaction = db_transaction();
  try {
    // Update existing records so that they are no longer the latest.
    db_update('vidhist')
      ->fields(array('latest' => 0))
      ->condition('uid', $data['uid'])
      ->condition('nid', $data['nid'])
      ->condition('latest', 1)
      ->execute();
    $id = db_insert('vidhist')
      ->fields(array(
        'uid' => $data['uid'],
        'nid' => $data['nid'],
        'htmlid' => 'vidhist-api',
        'latest' => 1,
        'start' => $data['start'],
        'end' => $data['end'],
        'duration' => $data['duration'],
        'viewtime' => $data['end'] - $data['start'],
        'url' => '',
        'complete' => 1,
        'token' => isset($data['token']) ? $data['token'] : strtoupper(user_password(32)),
        'created' => REQUEST_TIME,
        'updated' => REQUEST_TIME,
        'ip' => ip_address(),
      ))
      ->execute();

    module_invoke_all('vidhist_record_added', (object) $data);
  }
  catch (Exception $e) {
    $transaction->rollback();
    watchdog_exception('vidhist', $e);
  }
  return array(
    'id' => $data['nid'],
    //'uri' => services_resource_uri(array('vidhist', $id, $data['uid'])),
  );
}

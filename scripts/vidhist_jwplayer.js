/**
 * @file
 * Vidhist support for JWPlayer.
 *
 * Basically as long as there is a JWPlayer isntance on the page and it's
 * embeded using the jwplayer() JS call this code will find it and strack
 * playback.
 */

"use strict";

(function ($) {

/**
 * Register event listener for jwplayer API callbacks.
 */
Drupal.behaviors.vidHist_jwplayer = {
  attach: function (context, settings) {
    if (typeof Drupal.vidHist !== 'undefined' && typeof jwplayer !== 'undefined' && typeof jwplayer().onReady !== 'undefined') {
      var readyHelper = function (player) {
        // Ensure each player is registered just once to avoid duplicate data.
        if (!Drupal.vidHist.isPlayerRegistered($(player).attr('id'))) {
          var playerState = Drupal.vidHist.registerPlayer({
            htmlid: $(player).attr('id'),
            url: player.getPlaylistItem(),
            nid: settings.vidHist.nid,
            // Set this to -1 initially, we'll set it to an actual value
            // later.
            duration: -1,
            setPosition: function (position) {
              player.onReady(function() {
                player.seek(position);
              });
            },
            isPlaying: function () { var state = player.getState(); return state === 'BUFFERING' || state === 'PLAYING'; }
          });

          player.onTime(function () {
            // It takes jwplayer a fraction of a second to actually start
            // reporting the playhead position so we set the duration and
            // start position in the onTime event instead of onPlay since
            // sometimes the player doesn't yet know the duration.
            if (playerState.duration == -1) {
              playerState.noteStart(player.getPosition());
              playerState.duration = player.getDuration();
            }
          });

          player.onPause(function (oldstate) {
            playerState.noteStop(false);
          });

          player.onComplete(function () {
            playerState.noteStop(true);
          });

          if (typeof player.onSeek !== 'undefined') {
            // Use native onSeek() for jwplayer >= v5.6.1683.
            player.onSeek(function (params) {
              playerState.noteSeek(params.position);
            });
            player.onTime(function (params) {
              playerState.notePositionChange(params.position);
            });
          }
          else {
            // Emulate onSeek() when it's unavailable.
            player.onTime(function (params) {
              var position = params.position;
              if (typeof playerState.lastPosition !== 'undefined' && Math.abs(playerState.lastPosition - position) > 10) {
                playerState.noteSeek(position);
              }
              else {
                playerState.notePositionChange(position);
              }
              playerState.lastPosition = position;
            });
          }
        }
      };

      // Once the player is ready, attach vidhist functionality.
      jwplayer().onReady(readyHelper(jwplayer()));
    }
  }
};

})(jQuery);

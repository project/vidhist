<?php
/**
 * @file
 * Hook documentation for vidhist module.
 */

/**
 * Fired whenever a new vidhist record has been added to the database.
 *
 * This hooks allows other modules to respond to either the record being added
 * or to the data contained within the record. Note, this hook can fire
 * frequently depending on your vidhist configuration as often as once very
 * second during the lifetime of the page.
 *
 * @param object $data
 *   The vidhist record object. Contains the keys/values described in the
 *   vidhist table schema.
 */
function hook_vidhist_record_added($data) {
  if ($data->end == $data->duration) {
    have_a_party();
  }
}

<?php
/**
 * Filter handler for numeric formulas.
 */
class vidhist_handler_filter_numeric_formula extends views_handler_filter_numeric {
  function op_between($field) {
    $placeholder_min = $this->placeholder();
    $placeholder_max = $this->placeholder();
    if ($this->operator == 'between') {
      $this->query->add_where_expression($this->options['group'], "$field BETWEEN $placeholder_min AND $placeholder_max", array($placeholder_min => $this->value['min'], $placeholder_max => $this->value['max']));
    }
    else {
      $this->query->add_where_expression($this->options['group'], "$field <= $placeholder_min OR $field >= $placeholder_max", array($placeholder_min => $this->value['min'], $placeholder_max => $this->value['max']));
    }
  }

  function op_simple($field) {
    $placeholder = $this->placeholder();
    $this->query->add_where_expression($this->options['group'], "$field $this->operator $placeholder", array($placeholder => $this->value['value']));
  }

  function op_empty($field) {
    if ($this->operator == 'empty') {
      $operator = "IS NULL";
    }
    else {
      $operator = "IS NOT NULL";
    }
    $this->query->add_where_expression($this->options['group'], "$field $operator");
  }
}

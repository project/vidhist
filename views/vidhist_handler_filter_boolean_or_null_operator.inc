<?php
/**
 * Filter to handle matching of boolean values where NULL is always accepted.
 *
 * Definition items:
 * - label: (REQUIRED) The label for the checkbox.
 * - type: For basic 'true false' types, an item can specify the following:
 *    - true-false: True/false (this is the default)
 *    - yes-no: Yes/No
 *    - on-off: On/Off
 */
class vidhist_handler_filter_boolean_or_null_operator extends views_handler_filter_boolean_operator {
  // Whether to unconditionally accept NULL as a value or not.
  var $accept_null = FALSE;

  function init(&$view, &$options) {
    parent::init($view, $options);

    $this->accept_null = $this->options['accept_null'];
  }

  function option_definition() {
    $options = parent::option_definition();

    $options['accept_null'] = array('default' => FALSE);

    return $options;
  }

  function value_form(&$form, &$form_state) {
    parent::value_form($form, $form_state);
    $form['accept_null'] = array(
      '#type' => 'checkbox',
      '#title' => t('Accept any null values'),
      '#default_value' => $this->accept_null,
    );
  }

  function query() {
    $this->ensure_my_table();
    $field = "$this->table_alias.$this->real_field";
    $or = db_or();

    if (empty($this->value)) {
      $or->condition($field, 0, '=');
    }
    else {
      if (!empty($this->definition['use equal'])) {
        $or->condition($field, 1, '=');
      }
      else {
        $or->condition($field, 0, '<>');
      }
    }

    if ($this->accept_null) {
      $or->isNull($field);
    }

    $this->query->add_where($this->options['group'], $or);
  }
}

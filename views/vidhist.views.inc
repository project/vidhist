<?php
/**
 * @file
 * Video History Views data include file.
 */

/**
 * Implements hook_views_data().
 */
function vidhist_views_data() {
  // Basic table information.
  $data['vidhist']['table'] = array(
    // Create our own group.
    'group' => t('Video History'),
    // Enable vidhist as base table.
    'base' => array(
      'field' => 'vhid',
      'title' => t('Video History'),
      'help' => t('Video playback history maintained by vidhist module.'),
    ),
  );

  // Vidhist relationship with users base table.
  $data['users']['vidhist_user_relation'] = array(
    'group' => t('User'),
    'title' => t('Video History'),
    'help' => t('Associate users with video history records.'),
    // This relationship enables joins from users => vidhist.
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'label' => t('Video History'),
      'base' => 'vidhist',
      'base field' => 'uid',
      'relationship field' => 'uid',
    ),
  );

  // Vidhist relationship with node base table.
  $data['node']['vidhist_node_relation'] = array(
    'group' => t('Content'),
    'title' => t('Video History'),
    'help' => t('Associate nodes with video history records.'),
    // This relationship enables joins from node => vidhist.
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'label' => t('Video History'),
      'base' => 'vidhist',
      'base field' => 'nid',
      'relationship field' => 'nid',
    ),
  );

  // Add a percent viewed for current user field to the node table.
  $data['node']['vidhist_node_latest'] = array(
    'group' => t('Content'),
    'title' => t('Video Percent Viewed'),
    'help' => t('Percentage of this video the current user has watched. Only useful for nodes that have videos being tracked by vidhist.'),
    'field' => array(
      'handler' => 'vidhist_handler_field_user_percent_viewed',
      'click sortable' => TRUE,
    ),
  );

  // Add a resume/play link field for the current user to the node table.
  $data['node']['vidhist_node_resume'] = array(
    'group' => t('Content'),
    'title' => t('Video Resume Link'),
    'help' => t('Link to play/resume the video at the point the current user left off. Only useful for nodes that have videos being tracked by vidhist.'),
    'field' => array(
      'handler' => 'vidhist_handler_field_user_resume_link',
      'click sortable' => TRUE,
    ),
  );

  // UID field.
  $data['vidhist']['uid'] = array(
    'title' => t('User'),
    'help' => t('User watching video.'),
    'field' => array(
      'handler' => 'views_handler_field_user',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_user_name',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_user_uid',
    ),
    // This relationship enables joins from vidhist => user.
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'base' => 'users',
      'field' => 'uid',
      'label' => t('User'),
    ),
  );

  // NID field.
  $data['vidhist']['nid'] = array(
    'title' => t('Node'),
    'help' => t('Node containing video.'),
    'field' => array(
      'handler' => 'views_handler_field_node',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_node_nid',
    ),
    // This relationship enables joins from vidhist => node.
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'base' => 'node',
      'field' => 'nid',
      'label' => t('Node'),
    ),
  );

  // Htmlid field.
  $data['vidhist']['htmlid'] = array(
    'title' => t('Video HTML ID'),
    'help' => t('HTML ID of the video.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Asset field.
  $data['vidhist']['asset'] = array(
    'title' => t('Asset Identifier'),
    'help' => t('Video asset identifier.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // File field.
  $data['vidhist']['url'] = array(
    'title' => t('Video URL'),
    'help' => t('Video URL.'),
    'field' => array(
      'handler' => 'views_handler_field_url',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Start field.
  $data['vidhist']['start'] = array(
    'title' => t('Start time'),
    'help' => t('Time marker where this viewing started.'),
    'field' => array(
      'handler' => 'vidhist_handler_field_time',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // End field.
  $data['vidhist']['end'] = array(
    'title' => t('End time'),
    'help' => t('Time marker where this viewing ended.'),
    'field' => array(
      'handler' => 'vidhist_handler_field_time',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // Viewtime field.
  $data['vidhist']['viewtime'] = array(
    'title' => t('View time'),
    'help' => t('Amount of time that the video was watched.'),
    'field' => array(
      'handler' => 'vidhist_handler_field_time',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );

  // Duration field.
  $data['vidhist']['duration'] = array(
    'title' => t('Duration'),
    'help' => t('Total length of video in seconds.'),
    'field' => array(
      'handler' => 'vidhist_handler_field_time',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );

  // Complete field.
  $data['vidhist']['complete'] = array(
    'title' => t('Complete'),
    'help' => t('Video session is over.'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'vidhist_handler_filter_boolean_or_null_operator',
      'label' => t('Complete'),
      'type' => 'yes-no',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // Latest Record.
  $data['vidhist']['latest'] = array(
    'title' => t('Latest record'),
    'help' => t('Is this most recent history record for a user/node/video?'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'vidhist_handler_filter_boolean_or_null_operator',
      'label' => t('Latest record'),
      'type' => 'yes-no',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // Created field.
  $data['vidhist']['created'] = array(
    'title' => t('Created date'),
    'help' => t('The date the video history record was created.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
  );

  // Updated field.
  $data['vidhist']['updated'] = array(
    'title' => t('Updated date'),
    'help' => t('The date the video history record was last updated.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
  );

  // IP field.
  $data['vidhist']['ip'] = array(
    'title' => t('IP address'),
    'help' => t('IP address of user who watched the video.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Currently logged in user.
  $data['vidhist']['uid_current'] = array(
    'real field' => 'uid',
    'title' => t('Current User'),
    'help' => t('Filter the view to the currently logged in user.'),
    'filter' => array(
      'handler' => 'views_handler_filter_user_current',
      'type' => 'yes-no',
    ),
  );

  // Play and restart links.
  $data['vidhist']['play_link'] = array(
    'field' => array(
      'title' => t('Play link'),
      'help' => t('Link to play or restart the video.'),
      'handler' => 'vidhist_handler_field_play_link',
    ),
  );

  // Resume link.
  $data['vidhist']['resume_link'] = array(
    'field' => array(
      'title' => t('Resume link'),
      'help' => t('Link to resume a partially-played video.'),
      'handler' => 'vidhist_handler_field_resume_link',
    ),
  );

  // Percent viewed field.
  $data['vidhist']['percent_viewed'] = array(
    'title' => t('Percent viewed'),
    'help' => t('Percent of ending time marker to duration.'),
    'field' => array(
      'handler' => 'vidhist_handler_field_percent_viewed',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'vidhist_handler_filter_percent_viewed',
    ),
    'sort' => array(
      'handler' => 'vidhist_handler_sort_percent_viewed',
    ),
  );

  return $data;
}

<?php
/**
 * @file
 * Views field handler; Add a resume link for the node + current user.
 */

class vidhist_handler_field_user_resume_link extends vidhist_handler_field_resume_link {


  function query() {
    //parent:query();
    if (!isset($this->query->table_queue['vidhist'])) {
      // Join the vidhist table and ensure that this is the latest record for
      // this node from the current user.
      $join = new views_join;
      $join->construct('vidhist', 'node', 'nid', 'nid');
      $join->extra[] = array(
        'field' => 'latest',
        'operator' => '=',
        'value' => 1,
        'numeric' => TRUE,
      );
      $join->extra[] = array(
        'field' => 'uid',
        'operator' => '=',
        'value' => '***CURRENT_USER***',
        'numeric' => TRUE,
      );
      $this->query->add_relationship('vidhist', $join, 'vidhist');
    }
    // Add the fields we need.
    $this->query->add_field('vidhist', 'duration');
    $this->aliases['duration'] = 'vidhist_duration';

    $this->query->add_field('vidhist', 'end');
    $this->aliases['end'] = 'vidhist_end';

    $this->query->add_field('vidhist', 'htmlid');
    $this->aliases['htmlid'] = 'vidhist_htmlid';

    // Force the NID field as well.
    $this->query->add_field('node', 'nid');
    $this->aliases['nid'] = 'node_nid';
  }
}

<?php
/**
 * Filter handler for the percentage viewed (end / duration).
 */
class vidhist_handler_filter_percent_viewed extends vidhist_handler_filter_numeric_formula {
  function query() {
    $this->ensure_my_table();
    $alias = $this->table_alias;
    $field = "CASE WHEN $alias.duration = 0 THEN 0 ELSE ($alias.end / $alias.duration) * 100 END";

    $info = $this->operators();
    if (!empty($info[$this->operator]['method'])) {
      $this->{$info[$this->operator]['method']}($field);
    }
  }
}

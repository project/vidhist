<?php
/**
 * @file
 * Percent viewed field handler.
 */

/**
 * Field handler to display the percentage viewed (end / duration).
 */
class vidhist_handler_field_percent_viewed extends views_handler_field_numeric {
  /**
   * Class __construct().
   */
  function construct() {
    parent::construct();
    // Allow rounded output.
    $this->definition['float'] = TRUE;
  }

  /**
   * Override parent::option_definition().
   */
  function option_definition() {
    $options = parent::option_definition();

    $options['set_precision'] = array('default' => TRUE);
    $options['suffix'] = array('default' => '%', 'translatable' => TRUE);
    $options['display_as_graph'] = array('default' => FALSE);

    return $options;
  }

  /**
   * Override parent::options_form().
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['display_as_graph'] = array(
      '#title' => t('Display output formatted as a progress bar.'),
      '#type' => 'checkbox',
      '#default_value' => $this->options['display_as_graph'],
    );
  }

  /**
   * Override parent::query().
   */
  function query() {
    $this->ensure_my_table();
    $alias = $this->table_alias;
    // Add a field that contains the user's percentage watched based on whatever
    // the furthest point is that they have been in this video at any time, not
    // just the most recent viewing record.
    $field = "CASE WHEN $alias.duration = 0 THEN 0 ELSE ((SELECT end FROM vidhist WHERE uid = $alias.uid AND vidhist.nid = node.nid ORDER BY end DESC LIMIT 1) / $alias.duration) * 100 END";

    $this->field_alias = $this->query->add_field(NULL, $field, $this->table_alias . '_' . $this->field);
  }

  /**
   * Override parent::get_value().
   *
   * For values that are 95% or greater we just round up to 100%.
   *
   * @see vidhist_round_percent().
   */
  function get_value($values, $field = NULL) {
    $alias = isset($field) ? $this->aliases[$field] : $this->field_alias;
    if (isset($values->{$alias})) {
      return vidhist_round_percent($values->{$alias});
    }
  }

  /**
   * Override parent::render().
   */
  function render($values) {
    if ($this->options['display_as_graph']) {
      $value = $this->get_value($values);

      // Do not render if there is no {vidhist} data or if the user is not
      // logged in.
      if (empty($value) || user_is_anonymous()) {
        return;
      }

      return $this->make_graph($value);
    }

    return parent::render($values);
  }

  function make_graph($percent) {
    $css_added = &drupal_static(__FUNCTION__, FALSE);

    // Add vidhist CSS file once.
    if ($css_added === FALSE) {
      drupal_add_css(drupal_get_path('module', 'vidhist') . '/vidhist.css');
    }
    return '<div class="vidhist-progress-wrapper"><div class="vidhist-progress" style="display:block; width: ' . $percent . '%;"><span class="element-invisible">' . $percent . '</span></div></div>';
  }
}

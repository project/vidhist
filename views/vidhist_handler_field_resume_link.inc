<?php
/**
 * Field handler to present play/resume/restart links.
 */
class vidhist_handler_field_resume_link extends views_handler_field {
  function construct() {
    parent::construct();
    $this->additional_fields['nid'] = 'nid';
    $this->additional_fields['htmlid'] = 'htmlid';
    $this->additional_fields['end'] = 'end';
    $this->additional_fields['duration'] = 'duration';
  }

  function option_definition() {
    $options = parent::option_definition();

    $options['text_resume'] = array('default' => t('resume'), 'translatable' => TRUE);
    $options['text_play'] = array('default' => t('play'), 'translatable' => TRUE);

    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['text_resume'] = array(
      '#type' => 'textfield',
      '#title' => t('Resume Text'),
      '#description' => t('Text to display for user\'s when linking to a specific time in a partially watched video.'),
      '#default_value' => $this->options['text_play'],
    );
    $form['text_play'] = array(
      '#type' => 'textfield',
      '#title' => t('Play Text'),
      '#description' => t('Text to display for user\'s when they have already completed watchign the video and will start again from the beginning.'),
      '#default_value' => $this->options['text_play'],
    );
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function render($values) {
    return $this->render_link(NULL, $values);
  }

  function render_link($data, $values) {
    $nid = $values->nid;
    $htmlid = $this->sanitize_value($this->get_value($values, 'htmlid'));
    $end = $this->get_value($values, 'end');
    $duration = $this->get_value($values, 'duration');

    $last_position_exclusion_time = variable_get('vidhist_last_position_exclusion_time', VIDHIST_LAST_POSITION_EXCLUSION_TIME_DEFAULT);
    if ($duration - $end > $last_position_exclusion_time) {
      $text = !empty($this->options['text_resume']) ? $this->options['text_resume'] : t('resume');
    }
    else {
      $text = !empty($this->options['text_play']) ? $this->options['text_play'] : t('play');
      // Setting $end to 0 will cause the video to start from the beginning when
      // this link is clicked.
      $end = 0;
    }

    $this->options['alter']['make_link'] = TRUE;
    $this->options['alter']['link_class'] = 'vidhist-video-link vidhist-resume-link';
    $this->options['alter']['path'] = "node/$nid?position=$end";

    if (!empty($htmlid)) {
      $this->options['alter']['path'] = "node/$nid?htmlid=$htmlid&position=$end";
    }

    return $text;
  }

}

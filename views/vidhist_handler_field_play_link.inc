<?php
/**
 * Field handler to present play/resume/restart links.
 */
class vidhist_handler_field_play_link extends views_handler_field {
  function construct() {
    parent::construct();
    $this->additional_fields['nid'] = 'nid';
    $this->additional_fields['htmlid'] = 'htmlid';
    $this->additional_fields['end'] = 'end';
    $this->additional_fields['duration'] = 'duration';
  }

  function option_definition() {
    $options = parent::option_definition();

    $options['text_play'] = array('default' => t('play'), 'translatable' => TRUE);
    $options['text_restart'] = array('default' => t('restart'), 'translatable' => TRUE);

    return $options;
  }

  function options_form(&$form, &$form_state) {
    $form['text_play'] = array(
      '#type' => 'textfield',
      '#title' => t('Play Text'),
      '#description' => t('Text to display for user\'s who have completed the video. Links to beginning of video.'),
      '#default_value' => $this->options['text_play'],
    );
    $form['text_restart'] = array(
      '#type' => 'textfield',
      '#title' => t('Restart Text'),
      '#description' => t('Text to display for user\'s who have partially watched the video. Links to beginning of video.'),
      '#default_value' => $this->options['text_restart'],
    );
    parent::options_form($form, $form_state);
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function render($values) {
    return $this->render_link(NULL, $values);
  }

  function render_link($data, $values) {
    $last_position_exclusion_time = variable_get('vidhist_last_position_exclusion_time', VIDHIST_LAST_POSITION_EXCLUSION_TIME_DEFAULT);
    $nid = $this->sanitize_value($this->get_value($values, 'nid'));
    $htmlid = $this->sanitize_value($this->get_value($values, 'htmlid'));
    $end = $this->sanitize_value($this->get_value($values, 'end'));
    $duration = $this->get_value($values, 'duration');

    if (!empty($htmlid)) {
      if ($duration - $end > $last_position_exclusion_time) {
        $text = !empty($this->options['text_restart']) ? $this->options['text_restart'] : t('restart');
        $class = 'vidhist-video-link vidhist-restart-link';
      }
      else {
        $text = !empty($this->options['text_play']) ? $this->options['text_play'] : t('play');
        $class = 'vidhist-video-link vidhist-play-link';
      }

      $this->options['alter']['make_link'] = TRUE;
      $this->options['alter']['path'] = "node/$nid?htmlid=$htmlid&position=0";
      $this->options['alter']['link_class'] = $class;

      return $text;
    }
  }

}

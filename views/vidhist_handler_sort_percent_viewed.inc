<?php
/**
 * Sort handler for the percentage viewed (end / duration).
 */
class vidhist_handler_sort_percent_viewed extends views_handler_sort_date {
  function query() {
    $this->ensure_my_table();
    $alias = $this->table_alias;
    $field = "CASE WHEN $alias.duration = 0 THEN 0 ELSE ($alias.end / $alias.duration) * 100 END";

    $this->field_alias = $this->query->add_orderby(NULL, $field, $this->options['order'], $this->table_alias . '_' . $this->field);
  }
}

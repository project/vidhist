<?php
/**
 * @file
 * Views field handler. Display percentage of a video that the current user
 * has viewed.
 */

class vidhist_handler_field_user_percent_viewed extends vidhist_handler_field_percent_viewed {

  function get_value($values, $field = NULL) {
    $percent = 0;
    if ((isset($values->vidhist_duration) && $values->vidhist_duration != 0) && isset($values->vidhist_end)) {
      $percent = ($values->vidhist_end / $values->vidhist_duration) * 100;
    }
    return $percent;
  }

  function query() {
    if (!isset($this->query->table_queue['vidhist'])) {
      // Join the vidhist table so we can calculate percentage viewed, and ensure
      // that this is the latest record for this node from the current user.
      $join = new views_join;
      if (!empty($this->relationship)) {
        $table = $this->query->fields[$this->relationship . '_nid']['table'];
      }
      else {
        $table = 'node';
      }
      $join->construct('vidhist', $table, 'nid', 'nid');
      $join->extra[] = array(
        'field' => 'latest',
        'operator' => '=',
        'value' => 1,
        'numeric' => TRUE,
      );
      $join->extra[] = array(
        'field' => 'uid',
        'operator' => '=',
        'value' => '***CURRENT_USER***',
        'numeric' => TRUE,
      );
      $join->extra[] = array(
        'field' => 'uid',
        'operator' => '<>',
        'value' => 0,
        'numeric' => TRUE,
      );
      $this->query->add_relationship('vidhist', $join, 'vidhist');
    }
    // Add the two fields we need to make the calculation to the query.
    $this->query->add_field('vidhist', 'duration');
    $this->query->add_field('vidhist', 'end');
  }
}

<?php
/**
 * @file
 * Administrative pages and functions for vidhist module.
 */

/**
 * Form builder; Vidhist administrative settings form.
 */
function vidhist_settings() {
  $form = array();
  $period = drupal_map_assoc(array(5, 10, 15, 30, 60, 120, 180, 300, 600, 1200, 1800, 3600), 'format_interval');

  $form['vidhist_data_interval'] = array(
    '#type' => 'select',
    '#title' => t('Playhead save rate'),
    '#options' => array(0 => t('never')) + $period,
    '#description' => t("When a video is playing, how often should the playhead position be automatically saved to the server? Lower rates may affect site performance."),
    '#default_value' => variable_get('vidhist_data_interval', VIDHIST_DATA_INTERVAL_DEFAULT),
  );
  $form['vidhist_minimum_report_time'] = array(
    '#type' => 'select',
    '#title' => t('Minimum report time'),
    '#options' => array(0 => t('no minimum')) + $period,
    '#description' => t("What is the minimum duration of a play session in seconds? Play periods shorter than this will not be reported."),
    '#default_value' => variable_get('vidhist_minimum_report_time', VIDHIST_MINIMUM_REPORT_TIME_DEFAULT),
  );
  $form['vidhist_last_position_exclusion_time'] = array(
    '#type' => 'select',
    '#title' => t('Last position exclusion time'),
    '#options' => $period,
    '#description' => t("How close to the end is considered a full play? Play that falls within this window will not cause position setting when the user plays the video again."),
    '#default_value' => variable_get('vidhist_last_position_exclusion_time', VIDHIST_LAST_POSITION_EXCLUSION_TIME_DEFAULT),
  );
  return system_settings_form($form);
}

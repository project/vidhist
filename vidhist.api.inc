<?php

/**
 * @file
 * Provides vidhist database utilities.
 */

/**
 * Retrieves latest vidhist record for the supplied query conditions.
 *
 * @param (int) $uid
 *   User ID of the viewer.
 * @param (array) $options
 *   An associative array containing optional WHERE conditions:
 *   - nid: Node id containing video.
 *   - htmlid: HTML ID of the video container.
 *   - asset: Unique ID of the video.
 *   - complete: 0 or 1 indicating to filter by complete records only.
 *
 * @return (object)
 *   The latest database record for the user with supplied options or FALSE if
 *   no matching record is found.
 */
function vidhist_get_latest_record($uid, $options = array()) {
  $query = db_select('vidhist')
    ->fields('vidhist')
    ->condition('uid', $uid)
    ->condition('latest', 1)
    ->range(0, 1);
  foreach ($options as $key => $value) {
    $query->condition($key, $value);
  }

  return $query->execute()->fetchObject();
}

/**
 * Retrieves a latest vidhist record for each node ID provided.
 *
 * @param (int) $uid
 *   User ID of the viewer.
 * @param (array) $nids
 *   An array of node IDs containing video to filter on.
 *
 * @return (object)
 *   The latest database record for the user for each node ID or FALSE if
 *   no matching records are found.
 */
function vidhist_get_latest_node_records($uid, $nids) {
  if (empty($nids)) {
    return FALSE;
  }

  return db_select('vidhist')
    ->fields('vidhist')
    ->condition('uid', $uid)
    ->condition('latest', 1)
    ->condition('nid', $nids)
    ->execute()
    ->fetchAllAssoc('nid');
}

/**
 * Retrieve percentage of video a user has watched for a list of videos.
 *
 * Determine the furthest point in each video that the user has watched at any
 * point in history and then calculate the percentage of the video that has been
 * watched by comparing the furthest point the user has watched to the duration
 * of the video.
 *
 * Note: This function will round up high percentages to 100% in order to better
 * show when a user has completed a video.
 *
 * @param int $uid
 *   ID of the user to lookup records for.
 * @param array $nids
 *   Array of NIDS to retrieve video history records for.
 *
 * @return mixed
 *   An associative array keyed by node nid's containing information about a
 *   users viewing history for each node. The record returned represents the
 *   furthest point that the user has watched in the video at any time in their
 *   history.
 *
 * @see vidhist_round_percent().
 */
function vidhist_get_percent_watched($uid, $nids) {
  // There is no reason to look this up for UID = 0 since you can't really tell
  // a random anon. user how much video some other random anon user watched. And
  // it's way to expensive because anon has so many views. So instead we just
  // bail out.
  if ($uid === 0) {
    return FALSE;
  }

  if (empty($nids)) {
    return FALSE;
  }

  $query = db_query('SELECT v.uid, v.nid, v.duration, (SELECT end FROM {vidhist} WHERE uid = :inner_uid AND vidhist.nid = v.nid ORDER BY end DESC LIMIT 1) AS `end` FROM {vidhist} v WHERE uid = :uid AND nid IN (:nid) GROUP BY nid', array(':inner_uid' => $uid,':uid' => $uid, ':nid' => $nids));

  $results = $query->fetchAllAssoc('nid');
  foreach ($results as $nid => $row) {
    $percent = floor($row->end / $row->duration * 100);
    $results[$nid]->percent = vidhist_round_percent($percent);
  }

  return $results;
}

/**
 * Retrieve total time watched for the given video.
 *
 * @param (int) $nid
 *   Node ID of the video to retrieve total watched time for.
 * @param (int) $start
 *   Optional start timestamp. Only data recorded after the $start timestamp
 *   will be included in the total. Defaults to 0.
 * @param (int) $end
 *   Optional end timestamp. Only data recorded before the $end timestamp will
 *   be included in the total. Defaults to current time.
 * @param (bool) $reset
 *   Optionally reset the static cache.
 *
 * @return (int)
 *   The total time in seconds that a video has been watched in the given
 *   timeframe or FALSE if no data can be retrieved.
 */
function vidhist_viewtime_summary($nid, $start = NULL, $end = NULL, $reset = FALSE) {
  static $vidhist_summaries;

  if (!isset($vidhist_summaries) || $reset) {
    $vidhist_summaries = array();
  }

  $start = isset($start) ? $start : 0;
  $end = isset($end) ? $end : time();

  $key = $nid . ':' . $start . ':' . $end;

  if (!isset($vidhist_summaries[$key])) {
    $summary = db_query('SELECT SUM(viewtime) AS total_time FROM {vidhist} WHERE timestamp > :start AND timestamp < :end AND nid = :nid GROUP BY nid', array(':start' => $start, ':end' => $end, ':nid' => $nid))->fetchAssoc();
    if ($summary) {
      $vidhist_summaries[$key] = $summary['total_time'];
    }
  }

  return isset($vidhist_summaries[$key]) ? $vidhist_summaries[$key] : FALSE;
}

/**
 * Retrieve the total amount of video watched by a specific user.
 *
 * @param int $uid
 *   UID of the user to lookup history for.
 * @return mixed
 *   Number of seconds of video watched by this user or FALSE.
 */
function vidhist_user_total($uid) {
  $summary = db_query('SELECT SUM(viewtime) AS total_time FROM {vidhist} WHERE uid = :uid', array(':uid' => $uid))->fetchAssoc();
  return $summary['total_time'];
}

/**
 * Retrieve watched videos.
 *
 * @param int $uid
 *   Optional UID of the user to lookup videos for. Defaults to current user.
 * @return
 *   Array of video node IDs that the user has watched.
 */
function vidhist_user_watched($uid = NULL) {
  if (!isset($uid)) {
    global $user;
    $uid = $user->uid;
  }

  return db_query('SELECT DISTINCT nid FROM {vidhist} WHERE uid = :uid', array(':uid' => $uid))->fetchCol();
}
